
var routes = [
  {
    path: '/',
    url: './index.html',
  },
  {
    path: '/messages/:id/',
    url: './pages/messages.html',
  },
  {
    path: '/im/',
    url: './pages/im.html',

  },
  {
    path: '/user/:id/',
    componentUrl: './pages/user.html',
  },
  {
    path: '/friends/:id/',
    componentUrl: './pages/friends.html',
  },
  {
    path: '/settings/',
    url: './pages/settings.html',
  },
  {
    path: '/tabs/',
    url: './pages/tabs/index.html',
    tabs: [
      {
        path: '/',
        id: 'tab_messages',
        componentUrl: './pages/tabs/tab_messages.html',
      },
      {
        path: '/tab_friends/',
        id: 'tab_friends',
        componentUrl: './pages/tabs/tab_friends.html',
      },
      {
        path: '/tab_profile/',
        id: 'tab_profile',
        componentUrl: './pages/tabs/tab_profile.html',
      },
    ],
  },
  {
    path: '/search/',
    componentUrl: './pages/search.html',
  },
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
