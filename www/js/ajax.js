// Initialize Firebase
var config = {
    apiKey: "AIzaSyAoCmEK7ZhtVtyVUT5pm0vD487bNn8CXhE",
    authDomain: "fir-cfcba.firebaseapp.com",
    databaseURL: "https://fir-cfcba.firebaseio.com",
    projectId: "fir-cfcba",
    storageBucket: "fir-cfcba.appspot.com",
    messagingSenderId: "990224272555"
};
firebase.initializeApp(config);


var xhr;
var version_app = "1.0.1";

function ajax_api(method, data) {
    prom = null;


    if (xhr != undefined) xhr.abort();
    prom = new Promise(function (succeed, fail) {

        data[data.length, "id"] = window.localStorage.getItem("id");
        data[data.length, "key"] = window.localStorage.getItem("key");

        data = JSON.stringify(data);

        console.log("AJAX >> ");
        console.log(method);
        console.log(data);

        app.progressbar.show(app.theme === 'md' ? 'orange' : 'blue');

        xhr = app.request({
            url: HOST + "/api/?method=" + method,
            method: "POST",
            crossDomain: true,
            data: "data=" + data,
            beforeSend: function (XMLHttpRequest) {

                XMLHttpRequest.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        console.log("percentComplete: " + percentComplete);
                    }
                }, false);
            },
            error: function (response) {
                real_ajax_send = false;
                app.progressbar.hide();

                console.log("error");
                console.log(response);
                console.log(response["status"]);
                console.log(response["statusText"]);

                var t;

                if (method != 'messages.get.messages') {

                    if (response["status"] == 0) t = 'Ошибка подключения';
                    else
                    if (response["status"] == 504) t = 'Время ожидания вышло!';
                    else
                    if (response["status"] == 404) t = 'Сервер не найден';
                    else
                    if (response["status"] == 500) t = 'Ошибка сервера';
                    else t = 'error ' + response["status"] + ': ' + response["statusText"];

                    alert_app(t);
                }

                fail(response["status"]);
            },
            success: function (response) {
                real_ajax_send = false;

                console.log("AJAX << ");
                console.log(response);

                setTimeout(function () {
                    app.progressbar.hide();
                }, 1000);

                try {
                    response = JSON.parse(response);
                    if (response.success == 1) {
                        succeed(response);
                    } else {
                        app.dialog.close();
                        if (response.error.code == 3) alert_app('Ошибка: ' + response.error.messages);
                        if (response.error.code == 2) alert_app('Ошибка: ' + response.error.messages);
                        if (response.error.code == 1) alert_app('Ошибка авторизации');
                        app.dialog.close();
                    }
                } catch (e) {
                    fail('Ошибка данных сервера');
                    alert_app('Ошибка данных сервера');
                }

                response = null;
                delete response;
            },
            complete: function (response) {
                console.log("Окончание передачи");

            },
            start: function (response) {
                console.log("Старт передачи");
            }
        });
    });

    return prom;
}
