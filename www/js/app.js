var real_ajax_send, $$ = Dom7,
    HOST = "https://cp2019.abcget.ru",
    socket_on = !1,
    my_id = window.localStorage.getItem("id"),
    conversation_profile_user = !1,
    firebase_init = !0,
    activ_conversation_user = [],
    activ_chat_id = 0,
    init_messagebar_load = !1;

var app = new Framework7({
    root: "#app",
    id: "io.shat.groop",
    name: "Chat",
    theme: "md",
    routes: routes,
    input: {
        scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
        scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron
    },
    statusbar: {
        iosOverlaysWebView: !0,
        androidOverlaysWebView: !1
    },
    on: {
        init: function () {
            this.device.cordova && cordovaApp.init(this)
        }
    },
    view: {
        pushState: true,
        pushStateRoot: undefined,
        pushStateNoAnimation: false,
        pushStateSeparator: '#!',
    }
});

var view_main = app.views.get('.view-main');





null === my_id ? app.loginScreen.open(".login-screen", 1) : view_main.router.navigate("/tabs/");


function validateEmail(email) {
    var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(email);
}

// reg Screen
$$('.popup-reg .reg-button').on('click', function () {


    if (!validateEmail($$('.popup-reg [name="email"]').val())) {

        alert_app('Ошибка E-mail адреса');
        return false;
    }


    var formData = app.form.convertToData('.reg_f');


    ajax_api("auth.reg", formData).then(function (otv) {


        ////
        alert_app('Добро пожаловать', 2);


        if (otv.success == 1) {
            console.log("key: " + otv.key);
            console.log("id: " + otv.id);
            console.log("response: " + otv.response);

            // Установка параметров
            window.localStorage.setItem("key", otv.key);
            window.localStorage.setItem("id", otv.id);
            window.localStorage.setItem("user_name", otv.user_name);
            window.localStorage.setItem("user_avatar", otv.user_avatar);
            my_id = otv.id;


            console.log("id " + otv.id);
            console.log("key " + otv.key);


            view_main.router.navigate('/im/');

            app.popup.close();
            app.loginScreen.close('#my-login-screen');



        }



    }, function (error) {
        console.log(error);
        alert_app('Ошибка');
    });


});






// Login Screen
$$('#my-login-screen .login-button').on('click', function () {

    var formData = app.form.convertToData('.enter_f');


    ajax_api("auth.login", formData).then(function (otv) {


        $$('.reg .login-button').removeClass('disabled');

        ////
        alert_app('Добро пожаловать', 2);



        if (otv.success == 1) {
            console.log("key: " + otv.key);
            console.log("id: " + otv.id);
            console.log("response: " + otv.response);

            // Установка параметров
            window.localStorage.setItem("key", otv.key);
            window.localStorage.setItem("id", otv.id);
            window.localStorage.setItem("user_name", otv.user_name);
            window.localStorage.setItem("user_avatar", otv.user_avatar);
            my_id = otv.id;


            console.log("id " + otv.id);
            console.log("key " + otv.key);


            view_main.router.navigate('/im/');

            app.popup.close();
            app.loginScreen.close('#my-login-screen');







        }



    }, function (error) {
        console.log(error);
        $$('.reg .login-button').removeClass('disabled');
        alert_app('Ошибка++');
    });


});




/*
var view_im = app.views.get('.view-catalog');

*/

/*
view_main.router.navigate('/im/');


*/

function alert_app(e, t) {
    null == t && (t = 5), t *= 1e3, app.toast.create({
        text: e,
        closeTimeout: t
    }).open()
}

var messagebar;
var messages;

$$(document).on('page:init', '.page[data-name="im"]', function (e) {


    ajax_api("messages.get_list", {}).then(function (otv) {

            window.localStorage.setItem("user_name", otv.user_name);
            window.localStorage.setItem("user_avatar", otv.user_avatar);

            firebase_messages_init(otv.token_firebase);
            $$('.profile .user-name').html(otv.user_name);

            $$('.profile .user-ava').attr('src', otv.user_avatar);


            otv.data.forEach(function (item, i, arr) {

                var pr = (item["private"] == 1) ? '' : '-';



                $$('#view-im .chat-list ul').append('<li><a href="/messages/' + pr + item["id"] + '/" class="item-link item-content"><div class="item-media"><img src="' + item["avatar"] + '" width="44"></div><div class="item-inner"><div lass="item-title-row"><div class="item-title">' + item["title"] + '</div><div class="item-after">' + timeConverter(item["date"], true) + '</div></div><div class="item-text">' + item["from"] + ':</div><div class="item-subtitle">' + item["body"] + '</div></div></a></li>');

            });




        },
        function (error) {

        });


    $$("#view-friends").on('tab:show', function (event, ui) {
        var friendsList = app.virtualList.create({
            // List Element
            el: '.friends-list',
            // Pass array with items
            items: [],
            // Custom search function for searchbar
            searchAll: function (query, items) {
                var found = [];
                for (var i = 0; i < items.length; i++) {
                    if (items[i].title.toLowerCase().indexOf(query.toLowerCase()) >= 0 || query.trim() === '') found.push(i);
                }
                return found; //return array with mathced indexes
            },
            // List item Template7 template
            itemTemplate: ` <li>
                <div class="card">
                <div style="background-image:url({{avatar}})"
                    class="card-header align-items-flex-end friends-name white">{{name}}</div>
                <div class="card-footer">
                    <a href="#" class="link">Пригласить</a><a href="#" class="link">Написать</a><a href="#" data-id={{id}} class="friend-delete link">Удалить</a>
                </div>
            </div>
            </li>
            `
        });

        var requestsList = app.virtualList.create({
            el: '.friends-requests',
            items: [],
            itemTemplate: ` <li>
                <div class="card">
                <div style="background-image:url({{avatar}})"
                    class="card-header align-items-flex-end friends-name white">{{name}}</div>
                <div class="card-content">
                    <div class="card-content-link">Добавить в друзья</div>
                    <div class="card-content-link red">Отклонить заявку</div>
                </div>
            </div>
            </li>
            `
        });

        ajax_api("friends.getFriends", {}).then(function (response) {
                console.log(response)
                friendsList.appendItems(response.data)
                requestsList.appendItems(response.data.splice(2))

                if (!Object.keys(response.data).length) $$('#view-friends .searchbar-not-found').css('display', 'block');


                $$('.friend-delete').on('click', function (e) {
                    console.log($$(this).data('id'))
                });
            },
            function (error) {

            });


    });




    $$('.log-out').on('click', function () {
        app.dialog.confirm('Действительно выйти из аккаунта?', function () {
            app.dialog.alert('Пока!');
            localStorage.clear();
            app.loginScreen.open(".login-screen", 1)
        });
    });

    $$('.edit-name').on('click', function () {
        app.dialog.prompt('Введите Ваше новое имя', function (name) {
            app.dialog.confirm('Поменять имя на: ' + name + '?', function () {

                ajax_api("user.edit.name", {
                    name: name
                }).then(function (otv) {
                        $$('.profile .user-name').html(name);
                        app.dialog.alert('Теперь Вы: ' + name);
                    },
                    function (error) {});
            });
        });
    });

    $$('.edit-avatar').on('click', function () {
        app.sheet.create({
            el: '.load-ava-sheet-swipe-to-close',
            swipeToClose: true,
            backdrop: true,
        }).open();

    });





    $$('.edit-password').on('click', function () {

        app.dialog.create({
            title: 'Поменять пароль',
            text: '<div class="list no-hairlines-md"><p>Придумайте новый пароль и укажите его ниже.</p><ul><li class="item-content item-input item-input-with-info"><div class="item-inner"><div class="item-input-wrap"><input type="password" class="new_rn_pass" maxlength="99" placeholder=""><span class="input-clear-button"></span><div class="item-input-info">Новый пароль</div></div></div></li><li class="item-content item-input item-input-with-info"><div class="item-inner"><div class="item-input-wrap"><input type="password" class="new_rn2_pass" maxlength="99" placeholder=""><span class="input-clear-button"></span><div class="item-input-info">Новый пароль еще раз</div></div></div></li></ul></div>',
            buttons: [
                {
                    text: 'Отмена',
                    onClick: function () {
                        return 0;
                    }
                }, {
                    text: 'Продолжить',
                    close: false,
                    onClick: function () {


                        if ($$('.new_rn_pass').val().length <= 3) {
                            alert_app('Введенный пароль слишком короткий');
                            $$('.new_n_pass').focus();
                            return;
                        }
                        if ($$('.new_rn2_pass').val().length <= 3) {
                            alert_app('Введенный пароль слишком короткий');
                            $$('.new_n2_pass').focus();
                            return;
                        }
                        if ($$('.new_rn_pass').val() != $$('.new_rn2_pass').val()) {
                            alert_app('Введите одинаковые пароли в 2-х полях');
                            $$('.new_n_pass').focus();
                            return;
                        }


                        app.preloader.show();


                        ajax_api("auth.edit.pass", {
                            'password': $$('.new_rn_pass').val()
                        }).then(function (otv) {
                                app.preloader.hide();

                                if (otv.success) {
                                    alert_app('Пароль был изменен!');
                                    app.dialog.close();

                                    window.localStorage.setItem("id", otv.id);
                                    window.localStorage.setItem("key", otv.key);
                                    my_id = otv.id;

                                    app.popup.close();
                                    app.popup.close();
                                    app_init();

                                }
                                if (otv.error_title) {
                                    alert_app(otv.error_title);
                                }
                            },
                            function (error) {
                                app.preloader.hide();
                            });



                    }
                },
            ],
        }).open();




    });




});

$$(document).on('page:init', '.page[data-name="messages"]', function (e, page) {


    var id_chat = page.route.params.id;



    console.log('id_chat');
    console.log(id_chat);


    // Добавить в чат
    $$('.messages-page .person_add').on('click', function () {
        console.log('Собеседники');

        app.popover.close();
        app.popup.create({
            content: '<div class="popup person_add">' +
                '<div class="toolbar">' +
                '<div class="toolbar-inner">' +
                '<div class="left"></div>' +
                '<div class="right">' +
                '<a class="link popup-close">Закрыть</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="popup-modal-inner">' +
                '<div class="block ">' +
                '<div class="block-title">Добавить в чат друга:</div>' +
                '<div class="block block-strong text-align-center preloader_cont"><div class="preloader color-multi"><span class="preloader-inner"><span class="preloader-inner-gap"></span><span class="preloader-inner-left"><span class="preloader-inner-half-circle"></span></span><span class="preloader-inner-right"><span class="preloader-inner-half-ircle"></span></span>  </span></div></div>' +
                '<div class="list media-list person_add_list">' +
                '<ul>' +
                '</ul>' +
                '<div class="block float-right"><button class="col button button-outline chat_addFriend">Добавить</button></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
        }).open();


        ajax_api("friends.getFriends", {}).then(function (otv) {
                setTimeout(function () {
                    $$('.popup.person_add .preloader_cont').hide();
                    otv.friends.forEach(function (item, i, arr) {
                        $$('.person_add_list ul').append('<li>' +
                            '<label class="item-radio item-content">' +
                            '<input type="radio" name="user_id" value="' + item["id"] + '" checked />' +
                            '<i class="icon icon-radio"></i>' +
                            '<div class="item-media"><img src="' + item["avatar"] + '" width="44" /></div>' +
                            '<div class="item-inner">' +
                            '<div class="item-title-row">' +
                            '<div class="item-title">' + item["name"] + '</div>' +
                            '</div>' +
                            '<div class="item-subtitle">Человек</div>' +
                            '</div>' +
                            '</label>' +
                            '</li>');
                    });
                }, 1000);
            },
            function (error) {
                alert_app("Ошибка загрузки!");
            });



        $$('.chat_addFriend').on('click', function () {

            ajax_api("chat.addFriend", {
                'chat_id': id_chat,
                'user_id': $$('.person_add_list input[name="user_id"]').val(),
            }).then(function (otv) {


                    if (otv.error_title) alert_app(otv.error_title);
                    if (otv.adden) {
                        alert_app('Пользователь добавлен!');
                        app.popup.close();
                    }



                },
                function (error) {

                    alert_app("Ошибка загрузки!");

                });

        });
    });









    // Собеседники
    $$('.messages-page .popover-links .info').on('click', function () {
        console.log('Собеседники');



        app.popover.close();
        app.popup.create({
            content: '<div class="popup info-chat">' +
                '<div class="toolbar">' +
                '<div class="toolbar-inner">' +
                '<div class="left"></div>' +
                '<div class="right">' +
                '<a class="link popup-close">Закрыть</a>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="popup-modal-inner">' +
                '<div class="block no-margin">' +
                '<p class="margin-top"><b>' + $$('.messages-page .navbar .title').html() + '</b></p>' +
                '<p><b>Создан:</b> ' + timeConverter($$('.messages-page .navbar .date_create').html(), 1) + ' ' + timeConverter($$('.messages-page .navbar .date_create').html(), 0) + '</p>' +
                '<div class="block-title no-margin-left">Учасники:</div>' +
                '<div class="list media-list no-margin-left">' +
                '<ul>' +
                '<li>' +
                '<div class="block-strong text-align-center preloader_cont"><div class="preloader color-multi"><span class="preloader-inner"><span class="preloader-inner-gap"></span><span class="preloader-inner-left"><span class="preloader-inner-half-circle"></span></span><span class="preloader-inner-right"><span class="preloader-inner-half-ircle"></span>    </span>  </span></div></div></li>' +
                '</ul>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
        }).open();


        ajax_api("chat.getInfo", {
            'chat_id': id_chat
        }).then(function (otv) {

                setTimeout(function () {

                    $$('.popup.info-chat .preloader_cont').hide();

                    otv.data.forEach(function (item, i, arr) {

                        $$('.popup.info-chat .media-list ul').append('<li>' +
                            '<a href="' + item["id"] + '" class="item-link item-content">' +
                            '<div class="item-media"><img src="' + item["avatar"] + '" width="40"/></div>' +
                            '<div class="item-inner">' +
                            '<div class="item-title-row">' +
                            '<div class="item-title">' + item["name"] + '</div>' +
                            '</div>' +
                            '<div class="item-text">Был в сети:</div>' +
                            '</div></a></li>');
                    });

                }, 1000);


            },
            function (error) {

                alert_app("Ошибка загрузки!");

            });


    });


    // Выйти из чата
    $$('.messages-page .popover-links .exit').on('click', function () {

        app.popover.close();
        console.log('Выйти из чата');
        app.dialog.confirm('Действительно покинуть данный чат?', function () {
            ajax_api("chat.exit", {
                'chat_id': id_chat,
            }).then(function (otv) {
                    if (otv.del) {
                        view_main.router.navigate("/im/");
                        alert_app('Вы покинули чат!');
                    }

                },
                function (error) {
                    alert_app("Ошибка загрузки!");
                });

        });

    });

    messages = app.messages.create({
        el: '.messages',

        // First message rule
        firstMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if:
              - there is no previous message
              - or previous message type (send/received) is different
              - or previous message sender name is different
            */
            if (!previousMessage || previousMessage.type !== message.type || previousMessage.name !== message.name) return true;
            return false;
        },
        // Last message rule
        lastMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if:
              - there is no next message
              - or next message type (send/received) is different
              - or next message sender name is different
            */
            if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
            return false;
        },
        // Last message rule
        tailMessageRule: function (message, previousMessage, nextMessage) {
            // Skip if title
            if (message.isTitle) return false;
            /* if (bascially same as lastMessageRule):
      - there is no next message
      - or next message type (send/received) is different
      - or next message sender name is different
    */
            if (!nextMessage || nextMessage.type !== message.type || nextMessage.name !== message.name) return true;
            return false;
        }
    });

    // Init Messagebar
    messagebar = app.messagebar.create({
        el: '.messagebar'
    });



    var page = '0';
    load_conversation(page);

    function load_conversation(page_l) {

        if (id_chat < 0) {
            var formData = {
                'page': page_l,
                'chat_id': id_chat
            }
        } else {
            var formData = {
                'page': page_l,
                'user_id': id_chat
            }




            $$('.more_vert').hide();
            $$('.person_add').hide();
        }



        ajax_api("messages.get.messages", formData).then(function (otv) {
                page++;
                str = [];
                str_del = [];



                if (otv.data == '' && page_l == 0) $$('.wink').show();

                activ_conversation_user["avatar"] = otv.avatar;
                activ_conversation_user["name"] = otv.name;
                activ_chat_id = otv.chat_id;
                var temp = '';



                Array.prototype.insert = function (index, item) {
                    this.splice(index, 0, item);
                };
                /*
                                console.log('otv.data+');
                                console.log(otv.data);
                */
                $$('.messages-page .navbar .title').html(otv.name_chat);
                $$('.messages-page .navbar .date_create').html(otv.date_create);

                otv.data.forEach(function (item, i, arr) {
                    if (timeConverter(item["date"], true) != temp) {
                        otv.data.insert(i, {
                            text: (timeConverter(item["date"], true)),
                            isTitle: true
                        });
                    }
                    temp = timeConverter(item["date"], true);
                });

                otv.data.map(function (obj, index) {




                    obj.sender == my_id ? (obj.avatar = "", obj.name = "") : (obj.type = "received", obj.avatar = obj.avatar, obj.name = obj.name);


                    obj["textFooter"] = timeConverter(obj["date"], false);
                    obj["text"] = htotext(obj.text);
                    obj["attrs"] = {
                        'data-like': obj.like,
                        'data-id': obj.id,
                    }

                    return obj;
                });

                console.log("messages");
                console.log(otv.data);

                // Передаем Массив конструктуру myMessages

                //var met = (page_l == 0) ? 'append' : 'prepend';
                if (page_l == 0) {
                    var met = 'append';
                    var sm = true;
                } else {
                    var met = 'prepend';
                    otv.data.reverse();
                    var sm = false;
                }




                messages.addMessages(otv.data, met);
                setTimeout(function () {
                    app.toolbar.show('.toolbar');
                    messages.params.scrollMessages = true;
                }, 500);

                if (otv.data != '') infinite_scroll = true;
                $$('.conversation .infinite-scroll-preloader').hide();




                $$('.messages-content .message-received').each(function (i, elem) {
                    $$(elem).find('.message-content').append('<div class="wrapper wr-received"><a href="#" class="like-button" data-message="' + $$(this).data('id') + '"><i class="material-icons not-liked bouncy">favorite_border</i><i class="material-icons is-liked bouncy">favorite</i><span class="like-overlay"></span></a><p class="num">' + $$(this).data('like') + '</p></div>');
                });


                $$('.messages-content .message-sent').each(function (i, elem) {
                    $$(elem).find('.message-content').append('<div class="wrapper wr-sent"><a href="#" class="like-button" data-message="' + $$(this).data('id') + '"><i class="material-icons not-liked bouncy">favorite_border</i><i class="material-icons is-liked bouncy">favorite</i><span class="like-overlay"></span></a><p class="num">' + $$(this).data('like') + '</p></div>');
                });




                if (page_l == 0) {

                    setTimeout(function () {
                        messages.scroll();
                        app.lazy.create('.dialog_messages');
                    }, 2000);



                    $$('.dialog_messages').on('lazy:loaded', function () {
                        messages.scroll();
                    });


                } else app.lazy.create('.dialog_messages');




            },
            function (error) {
                console.log(error);
                infinite_scroll = true;
                //load_local_conversation(user, page);
            });

    }


    //MessageBar.addMessages(otv.data, met);



    // Response flag
    var responseInProgress = false;

    // Send Message
    $$('.send-link').on('click', function () {
        var text = messagebar.getValue().replace(/\n/g, '<br>').trim();
        // return if empty message
        if (!text.length) return;

        // Clear area
        messagebar.clear();

        // Return focus to area
        messagebar.focus();

        // Add message to messages
        messages.addMessage({
            text: text,
            textFooter: timeConverter(Date.now(), false)
        });

        if (responseInProgress) return;
        // Receive dummy message


        $$(".messages-page .message-last").eq(-1).css('opacity', '0.4');

        var formData = {
            text: text,
            to: id_chat,
        }

        ajax_api("message.send", formData).then(function (otv) {
                console.log(otv);
                $$(".messagebar-area textarea").removeClass('disabled');
                $$(".messages-page .message-last").eq(-1).css('opacity', '1');

                setTimeout(function () {
                    $$('.messages-content .message-sent').eq(-1).find('.message-content').append('<div class="wrapper wr-sent"><a href="#" class="like-button"><i class="material-icons not-liked bouncy">favorite_border</i><i class="material-icons is-liked bouncy">favorite</i><span class="like-overlay"></span></a><p class="num">5</p></div>');

                }, 300);


            },
            function (error) {
                $$(".messages-page .message-last .message-bubble").eq(-1).css('background', '#f95d8f');

            });

    });



    function receiveMessage() {
        responseInProgress = true;
        setTimeout(function () {
            // Get random answer and random person
            var answer = answers[Math.floor(Math.random() * answers.length)];
            var person = people[Math.floor(Math.random() * people.length)];

            // Show typing indicator
            messages.showTyping({
                header: person.name + ' печатает...',
                avatar: person.avatar
            });

            setTimeout(function () {
                // Add received dummy message
                messages.addMessage({
                    text: answer,
                    type: 'received',
                    name: person.name,
                    avatar: person.avatar,
                    textFooter: 'только что'
                });
                // Hide typing indicator
                messages.hideTyping();
                responseInProgress = false;

                $$('.messages-content .message-received').eq(-1).find('.message-content').append('<div class="wrapper"><a href="#" class="like-button"><i class="material-icons not-liked bouncy">favorite_border</i><i class="material-icons is-liked bouncy">favorite</i><span class="like-overlay"></span></a></div>');

            }, 4000);
        }, 1000);
    }



    $$('.messages-content').on('click', '.like-button', function (event) {

        var t = this;
        $$(t).css('opacity', '0.3');

        ajax_api("like.toggle", {
            message_id: $$(this).data('message')
        }).then(function (otv) {

                $$(t).css('opacity', '1');
                alert_app(otv.title, 1);


                $$(t).closest(".wrapper").find('.num').html(otv.total);

                //$$(t).toggleClass('is-active');

            },
            function (error) {

                $$(t).css('background', '#f95d8f');

            });




    });


});





function timeConverter(e, t) {
    var a = new Date(1e3 * e),
        n = a.getFullYear(),
        d = a.getMonth(),
        r = a.getDate(),
        g = addZero(a.getHours()),
        o = addZero(a.getMinutes());
    addZero(a.getSeconds());
    d = 10 == ++d || 11 == d || 12 == d ? d : "0" + d, r = 9 <= r ? r : "0" + r;
    var w = new Date;
    return new_date = w.getDate() + "." + (w.getMonth() + 1) + "." + w.getFullYear(), t ? r + "." + d + "." + n == new_date ? "Сегодня" : r + 1 + "." + d + "." + n == new_date ? "Вчера" : r + "." + d + "." + n : (r + "." + d + "." + n == new_date || new_date, g + ":" + o)
}

function addZero(n) {
    return n < 10 && (n = "0" + n), n
}




function htotext(e) {
    return e.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;").replace(/\n/g, "<br>").replace("↵", "<br>")
}

function htotext_nb(e) {
    return e.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;").replace("↵", "<br>")
}




function firebase_messages_init(token) {

    firebase.auth().signInWithCustomToken(token).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;


        app.dialog.create({
            title: 'Ошибка!',
            text: 'При подключении произошла ошибка: ' + errorCode,
            buttons: [{
                text: 'Перезагрузить',
                onClick: function () {
                    location.reload();
                }
            }, ],
        }).open();

    }).then(function (data_t) {

        console.log("google user: " + data_t.user.uid);

        var db_ref = firebase.database().ref('chat');

        db_ref.endAt().limitToLast(1).on('child_changed', function (data_message) {

            console.log("google data_message: " + data_message.val());
            console.log(data_message.val());


            var q = data_message.val();
            var keys = Object.keys(q);
            var m = q[keys[keys.length - 1]];
            console.log('data_message.val()');
            console.log(m);

            if (m.to == activ_chat_id) new_message(m);

        });


    });

}


function new_message(data) {
    if (data.from != my_id) {


        messages.addMessage({
            type: 'received',
            name: data.name,
            avatar: data.avatar,
            textFooter: timeConverter(data.date, false),
            text: escapeText(data.text.replace(/\n/g, '<br>')),
        });
    }
}


function escapeText(e) {
    return e = (e = (e = (e = (e = (e = (e = e.replace(new RegExp("&lt;", "g"), "<")).replace(new RegExp("&gt;", "g"), ">")).replace(new RegExp("&quot;", "g"), '"')).replace(new RegExp("&#39;", "g"), "'")).replace(new RegExp("&#x2F;", "g"), "/")).replace(new RegExp("&#x60;", "g"), "`")).replace(new RegExp("&#x3D;", "g"), "=")
}
